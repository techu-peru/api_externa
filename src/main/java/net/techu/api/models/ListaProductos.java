package net.techu.api.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaProductos implements Serializable {
private String texto;

//@JsonProperty("value")
@SerializedName("value")
private List<Producto> value;

public ListaProductos(){

}
    public ListaProductos(String texto, List<Producto> value) {
        this.texto = texto;

        this.value = value;

    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public List<Producto> getValue() {
        return value;
    }

    public void setValue(List<Producto> value) {

        this.value = value;
    }
}
