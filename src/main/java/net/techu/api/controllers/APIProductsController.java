package net.techu.api.controllers;

import com.google.gson.Gson;
import net.techu.api.GestorRest;
import net.techu.api.models.ListaProductos;
import net.techu.api.models.Producto;
import org.springframework.asm.Type;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
public class APIProductsController {
    @GetMapping(value="/apiproductos")
    public List<Producto> getProducts(){
        //return "Hola";
        final String uri="https://services.odata.org/V4/OData/OData.svc/Products?$select=ID,Name,Price";
        //validando cabecera
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Usuario","AppTechU");
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        //
        GestorRest gestor = new GestorRest();
        RestTemplate plantilla = gestor.crearClienteRest(new RestTemplateBuilder());
        /*
        ResponseEntity<ListaProductos> response = plantilla.exchange(uri, HttpMethod.GET, null,
                new ParameterizedTypeReference<ListaProductos>() {
                });
        List<Producto> data = response.getBody().getValue();
        return data;*/

        String resp=plantilla.getForObject(uri,String.class);

        final Gson gson = new Gson();
        //final Type tipoListaEmpleados = new TypeToken<List<Empleado>>(){}.getType();
        final ListaProductos productos = gson.fromJson(resp, ListaProductos.class);
        
        return productos.getValue();

    }


}
